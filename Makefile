all:
	dune build

test:
	dune build @runtest

clean:
	dune clean
