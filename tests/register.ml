open Obnox

type Error.error += Dummy
let () =
  Error.register_error
    ~id:"dummy"
    ~payload_of_error:(function | Dummy -> Some () | _ -> None)
    ~error_of_payload:(fun () -> Dummy)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "dummy")
    ~user_info:Data_encoding.null

let main () = ()
