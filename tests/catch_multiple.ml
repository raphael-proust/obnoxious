open Obnox

type Error.error += To_be_catched of int
let () =
  Error.register_error
    ~id:"to_be_catched"
    ~payload_of_error:(function | To_be_catched i -> Some i | _ -> None)
    ~error_of_payload:(fun i -> To_be_catched i)
    ~pp_payload:(fun fmt i -> Format.fprintf fmt "to_be_catched(%d)" i)
    ~user_info:Data_encoding.int31

type Error.error += To_be_ignored
let () =
  Error.register_error
    ~id:"to_be_ignored"
    ~payload_of_error:(function | To_be_ignored -> Some () | _ -> None)
    ~error_of_payload:(fun () -> To_be_ignored)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "to_be_ignored")
    ~user_info:Data_encoding.null

type Error.error += Wat
let () =
  Error.register_error
    ~id:"wat"
    ~payload_of_error:(function | Wat -> Some () | _ -> None)
    ~error_of_payload:(fun () -> Wat)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "wat")
    ~user_info:Data_encoding.null

let main_catch () =
  let open Traced in
  let r =
     (both
       (both (return 0) (error @@ To_be_catched 0))
       (both (return 0) (error @@ To_be_catched 1))
     >>= fun ((q, w), (e, r)) -> return (q + w + e + r))
     |> catch_exists (function Wat -> Some (return (-1)) | _ -> None)
     |> catch_exists (function To_be_catched i -> Some (return i) | _ -> None)
  in
  match r with
  | Error _ -> assert false
  | Ok 0 | Ok 1 -> ()
  | Ok _ -> assert false

let main_miss () =
  let open Traced in
  let r =
     (both
       (both (return 0) (return 1))
       (both (error To_be_ignored) (error To_be_ignored))
     >>= fun ((q, w), (e, r)) -> return (q + w + e + r))
     |> catch_exists (function Wat -> Some (return (-1)) | _ -> None)
     |> catch_exists (function To_be_catched i -> Some (return i) | _ -> None)
  in
  match r with
  | Error _ -> ()
  | Ok _ -> assert false

let main () =
   main_catch ();
   main_miss ()
