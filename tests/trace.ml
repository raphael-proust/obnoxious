open Obnox

type Error.error += A
let () =
  Error.register_error
    ~id:"a"
    ~payload_of_error:(function | A -> Some () | _ -> None)
    ~error_of_payload:(fun () -> A)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "a")
    ~user_info:Data_encoding.null

type Error.error += B
let () =
  Error.register_error
    ~id:"b"
    ~payload_of_error:(function | B -> Some () | _ -> None)
    ~error_of_payload:(fun () -> B)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "b")
    ~user_info:Data_encoding.null

let main () =
  let open Traced in
  begin
    return () >>= fun () ->
    error A |>
    trace A |>
    trace B |>
    trace B |>
    trace A >>= fun () ->
    return ()
  end
  |> recover
    (fun trace ->
       assert (Trace.to_errors trace = S (A, S (B, S (B, S (A, E A))))))
