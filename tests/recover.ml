open Obnox

type Error.error += To_be_recovered
let () =
  Error.register_error
    ~id:"to_be_recoverd"
    ~payload_of_error:(function | To_be_recovered -> Some () | _ -> None)
    ~error_of_payload:(fun () -> To_be_recovered)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "to_be_recoverd")
    ~user_info:Data_encoding.null

let main () =
  let open Traced in
  (error To_be_recovered)
  |> recover (fun trace -> assert (Trace.to_errors trace = E To_be_recovered))
