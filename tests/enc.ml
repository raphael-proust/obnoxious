open Obnox

let de_round_trip e v =
  let open Data_encoding in
  let e = Traced.encoding e in
  let b = Binary.to_bytes_exn e v in
  let u = Binary.of_bytes_exn e b in
  v = u

let main () =
  assert (de_round_trip Data_encoding.string (Traced.return ""));
  assert (de_round_trip Data_encoding.string (Traced.return "\x000"));
  assert (de_round_trip Data_encoding.string (Traced.return "foo bar"));
  assert (de_round_trip Data_encoding.string
            (Traced.error (Error.Invalid_arg "nope")));
  assert (de_round_trip Data_encoding.string
            Traced.(
              error (Error.Invalid_arg "nope")
              |> trace (Error.Invalid_arg "nopetynopetynope")
            ));
  assert (de_round_trip Data_encoding.string
            (Error Trace.(conp [
                 (trace (Error.Invalid_arg "foo"));
                 (trace (Error.Invalid_arg "bar"));
                 (trace (Error.Invalid_arg "far"));
                 (trace (Error.Invalid_arg "boo"));
               ])));
  assert (de_round_trip Data_encoding.string
            (Error Trace.(conp [
                 cons (Error.Invalid_arg "foo") (trace (Error.Invalid_arg "bar"));
                 cons (Error.Invalid_arg "far") (trace (Error.Invalid_arg "boo"));
               ])));
  ()
