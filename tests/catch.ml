open Obnox

type Error.error += To_be_caught
let () =
  Error.register_error
    ~id:"to_be_caught"
    ~payload_of_error:(function | To_be_caught -> Some () | _ -> None)
    ~error_of_payload:(fun () -> To_be_caught)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "to_be_caught")
    ~user_info:Data_encoding.null

let main () =
  let open Traced in
  let r =
     (error To_be_caught)
     |> catch_exists (function To_be_caught -> Some (return ()) | _ -> None)
  in
  match r with
  | Error _ -> assert false
  | Ok () -> ()
