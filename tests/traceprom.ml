open Obnox

type Error.error += C
let () =
  Error.register_error
    ~id:"c"
    ~payload_of_error:(function | C -> Some () | _ -> None)
    ~error_of_payload:(fun () -> C)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "c")
    ~user_info:Data_encoding.null

type Error.error += D
let () =
  Error.register_error
    ~id:"d"
    ~payload_of_error:(function | D -> Some () | _ -> None)
    ~error_of_payload:(fun () -> D)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "d")
    ~user_info:Data_encoding.null

let main () =

  let pt =
    let open Promise in
      begin
        return () >>= fun () ->
       error C |>
       trace C |>
       trace D |>
       trace D |>
       trace C >>= fun () ->
       return ()
      end
    |> recover
      (fun trace ->
         assert (Trace.to_errors trace = S (C, S (D, S (D, S (C, E C)))));
         Lwt.return_unit)
  in

  match Lwt.state pt with
  | Lwt.Return () -> ()
  | Lwt.Fail _
  | Lwt.Sleep -> assert false
