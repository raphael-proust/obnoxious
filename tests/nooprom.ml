let main () =
  let pt =
    let open Obnox.Promise in
    (return ())
    |> recover (fun _ -> assert false) in

  match Lwt.state pt with
  | Lwt.Return () -> ()
  | Lwt.Fail _
  | Lwt.Sleep -> assert false
