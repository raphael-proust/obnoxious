open Obnox

type Error.error += Left_fail
let () =
  Error.register_error
    ~id:"left_fail"
    ~payload_of_error:(function | Left_fail -> Some () | _ -> None)
    ~error_of_payload:(fun () -> Left_fail)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "left_fail")
    ~user_info:Data_encoding.null

type Error.error += Right_fail
let () =
  Error.register_error
    ~id:"right_fail"
    ~payload_of_error:(function | Right_fail -> Some () | _ -> None)
    ~error_of_payload:(fun () -> Right_fail)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "right_fail")
    ~user_info:Data_encoding.null

let main () =

  let open Promise in
  let open Extended.List in

  let input = [ `Ok 0 ; `Ok 1 ; `Left ; `Left ; `Right ; `Ok 22 ; `Ok 0 ; `Left ] in
  let f = function
    | `Ok x -> return x
    | `Left -> error Left_fail
    | `Right -> error Right_fail
  in

  let pp =
    (map_p input f)
    |> recover
      (fun trace ->
         begin
           match Trace.to_errors trace with
           | P ts -> assert (List.length ts = 4)
           | _ -> assert false
         end;
         Lwt.return_nil) in
  let ps =
    (map_s input f)
    |> recover
      (fun trace ->
         begin
           match Trace.to_errors trace with
           | E Left_fail -> ()
           | _ -> assert false
         end;
         Lwt.return_nil) in

  begin match Lwt.state pp with
    | Lwt.Return [] -> ()
    | Lwt.Return _
    | Lwt.Fail _
    | Lwt.Sleep -> assert false
  end;
  begin match Lwt.state ps with
    | Lwt.Return [] -> ()
    | Lwt.Return _
    | Lwt.Fail _
    | Lwt.Sleep -> assert false
  end;
