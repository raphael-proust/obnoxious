open Obnox

type Error.error += Needs_recovery of int
let () =
  Error.register_error
    ~id:"needs_recovery"
    ~payload_of_error:(function | Needs_recovery i -> Some i | _ -> None)
    ~error_of_payload:(fun i -> Needs_recovery i)
    ~pp_payload:(fun fmt i -> Format.fprintf fmt "needs_recovery(%d)" i)
    ~user_info:Data_encoding.int31

type Error.error += Cant_be_recovered
let () =
  Error.register_error
    ~id:"needs_recovery"
    ~payload_of_error:(function | Cant_be_recovered -> Some () | _ -> None)
    ~error_of_payload:(fun () -> Cant_be_recovered)
    ~pp_payload:(fun fmt () -> Format.fprintf fmt "cant_be_recovered")
    ~user_info:Data_encoding.unit

type r = Ok_through of int | Recovered of int

let main_full_recovery () =
  let open Traced in
  let r =
     (both (error @@ Needs_recovery 1) (error @@ Needs_recovery 1)
     >>= fun _ -> return (-1))
     |> catch_fold
           (fun e i -> match e with
              | Needs_recovery j -> Some (Ok (i + j))
              | _ -> None)
           0
  in
  match r with
  | Ok 2 -> ()
  | Ok _ -> assert false
  | Error _ -> assert false

let main_no_recovery () =
  let open Traced in
  let r =
     (both (error @@ Needs_recovery 1) (error @@ Cant_be_recovered)
     >>= fun _ -> return (-1))
     |> catch_fold
           (fun e i -> match e with
              | Needs_recovery j -> Some (Ok (i + j))
              | _ -> None)
           0
  in
  match r with
  | Ok _ -> assert false
  | Error _ -> ()

let main () =
   main_full_recovery ();
   main_no_recovery ()

