module type CONSTRAINTS = sig
   (* for constraining/substituting with [Error.t] *)
   type error = ..

   (* for constraining/substituting with [Traced.t] *)
   type 'a traced

   (* for constraining/substituting with [Traced.t] *)
   type trace

end

module type BASE = sig
   module Error : Traced.Sigs.ERROR
   module Trace : Traced.Sigs.TRACE
      with type error := Error.error
   module Traced : Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result

   type Error.error += Invalid_argument_ of string

   type Error.error += Not_found_

end


module type LIST = sig

   include CONSTRAINTS

   val length : 'a list -> int
   val compare_lengths : 'a list -> 'b list -> int
   val compare_length_with : 'a list -> int -> int
   val cons : 'a -> 'a list -> 'a list
   val hd : 'a list -> 'a traced
   val tl : 'a list -> 'a list traced
   val nth: 'a list -> int -> 'a traced
   val rev : 'a list -> 'a list
   val init : int -> (int -> 'a) -> 'a list
   val append : 'a list -> 'a list -> 'a list
   val rev_append : 'a list -> 'a list -> 'a list
   val concat : 'a list list -> 'a list
   val flatten : 'a list list -> 'a list
   val map : ('a -> 'b) -> 'a list -> 'b list
   val mapi : (int -> 'a -> 'b) -> 'a list -> 'b list
   val map_e : ('a -> 'b traced) -> 'a list -> 'b list traced
   val mapi_e : (int -> 'a -> 'b traced) -> 'a list -> 'b list traced
   val rev_map : ('a -> 'b) -> 'a list -> 'b list
   val rev_map_e : ('a -> 'b traced) -> 'a list -> 'b list traced
   val rev_mapi_e : (int -> 'a -> 'b traced) -> 'a list -> 'b list traced
   val map2 : ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list
(*    val mapi2 : (int -> 'a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list *)
   val map2_e : ('a -> 'b -> 'c traced) -> 'a list -> 'b list -> 'c list traced
   val mapi2_e : (int -> 'a -> 'b -> 'c traced) -> 'a list -> 'b list -> 'c list traced
   val iter : ('a -> unit) -> 'a list -> unit
   val iteri : (int -> 'a -> unit) -> 'a list -> unit
(*    val iter_e : ('a -> unit traced) -> 'a list -> unit traced *)
(*    val iteri_e : (int -> 'a -> unit traced) -> 'a list -> unit traced *)
   val iter2 : ('a -> 'b -> unit) -> 'a list -> 'b list -> unit
(*    val iteri2 : (int -> 'a -> 'b -> unit) -> 'a list -> 'b list -> unit *)
(*    val iter2_e : ('a -> 'b -> unit traced) -> 'a list -> 'b list -> unit traced *)
(*    val iteri2_e : (int -> 'a -> unit traced) -> 'a list -> 'b list -> unit traced *)
   val fold_left : ('a -> 'b -> 'a) -> 'a -> 'b list -> 'a
   val fold_right : ('a -> 'b -> 'b) -> 'a list -> 'b -> 'b
(*    val fold_left_e : ('a -> 'b -> 'a traced) -> 'a -> 'b list -> 'a traced *)
(*    val fold_right_e : ('a -> 'b -> 'b traced) -> 'a list -> 'b -> 'b traced *)
   val fold_left2 : ('a -> 'b -> 'c -> 'a) -> 'a -> 'b list -> 'c list -> 'a
   val fold_right2 : ('a -> 'b -> 'c -> 'c) -> 'a list -> 'b list -> 'c -> 'c
(*    val fold_left2_e : ('a -> 'b -> 'c -> 'a traced) -> 'a -> 'b list -> 'c list -> 'a traced *)
(*    val fold_right2_e : ('a -> 'b -> 'c -> 'c traced) -> 'a list -> 'b list -> 'c -> 'c traced *)
   val for_all : ('a -> bool) -> 'a list -> bool
   val exists : ('a -> bool) -> 'a list -> bool
   val for_all2 : ('a -> 'b -> bool) -> 'a list -> 'b list -> bool traced
   val exists2 : ('a -> 'b -> bool) -> 'a list -> 'b list -> bool traced
(*    val for_all_e : ('a -> bool traced) -> 'a list -> bool traced *)
(*    val exists_e : ('a -> bool traced) -> 'a list -> bool traced *)
(*    val for_all2_e : ('a -> 'b -> bool traced) -> 'a list -> 'b list -> bool traced *)
(*    val exists2_e : ('a -> 'b -> bool traced) -> 'a list -> 'b list -> bool traced *)
   val mem : 'a -> 'a list -> bool
   val memq : 'a -> 'a list -> bool
   val find : ('a -> bool) -> 'a list -> 'a traced
   val find_e : ('a -> bool traced) -> 'a list -> 'a traced
   val filter : ('a -> bool) -> 'a list -> 'a list
(*    val filter_e : ('a -> bool traced) -> 'a list -> 'a list traced *)
   val find_all : ('a -> bool) -> 'a list -> 'a list
(*    val find_all_e : ('a -> bool traced) -> 'a list -> 'a list traced *)
   val partition : ('a -> bool) -> 'a list -> 'a list * 'a list
(*    val partition_e : ('a -> bool traced) -> 'a list -> ('a list * 'a list) traced *)
   val assoc : 'a -> ('a * 'b) list -> 'b traced
   val assq : 'a -> ('a * 'b) list -> 'b traced
   val mem_assoc : 'a -> ('a * 'b) list -> bool
   val mem_assq : 'a -> ('a * 'b) list -> bool
   val remove_assoc : 'a -> ('a * 'b) list -> ('a * 'b) list
   val remove_assq : 'a -> ('a * 'b) list -> ('a * 'b) list
   val split : ('a * 'b) list -> 'a list * 'b list
   val combine : 'a list -> 'b list -> ('a * 'b) list
   val sort : ('a -> 'a -> int) -> 'a list -> 'a list
   val stable_sort : ('a -> 'a -> int) -> 'a list -> 'a list
   val fast_sort : ('a -> 'a -> int) -> 'a list -> 'a list
   val sort_uniq : ('a -> 'a -> int) -> 'a list -> 'a list
   val merge : ('a -> 'a -> int) -> 'a list -> 'a list -> 'a list

end

module type STDLIB = sig

   include CONSTRAINTS

   include BASE

   module List : LIST
      with type error := error
      and type trace := trace
      and type 'a traced := 'a traced

end
