module MkBase
   (Error : Traced.Sigs.ERROR)
   (Trace : Traced.Sigs.TRACE with type error := Error.error)
   (Traced : Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result)
   (User : sig
      val invalid_arg_user_info : string Error.user_t
      val not_found_user_info : unit Error.user_t
   end)
: Sigs.BASE
   with module Error = Error
   and module Trace = Trace
   and module Traced = Traced
= struct

   module Error = Error
   module Trace = Trace
   module Traced = Traced

   type Error.error += Invalid_argument_ of string

   let () =
      Error.register_error ~id:"invalid_argument"
         ~payload_of_error:(function Invalid_argument_ s -> Some s | _ -> None)
         ~error_of_payload:(fun s -> Invalid_argument_ s)
         ~pp_payload:(fun fmt msg -> Format.fprintf fmt "Invalid argument: %s" msg)
         ~user_info:User.invalid_arg_user_info

   type Error.error += Not_found_

   let () =
      Error.register_error ~id:"not_found"
         ~payload_of_error:(function Not_found_ -> Some () | _ -> None)
         ~error_of_payload:(fun () -> Not_found_)
         ~pp_payload:(fun fmt () -> Format.fprintf fmt "Not found")
         ~user_info:User.not_found_user_info

end

module List (Base: Sigs.BASE)
: Sigs.LIST
   with type error := Base.Error.error
   and type trace := Base.Trace.trace
   and type 'a traced := 'a Base.Traced.t
= struct

   open Base.Traced

   include List
   let hd l = try Ok (hd l) with Failure s -> error (Base.Invalid_argument_ s)
   let tl l = try Ok (tl l) with Failure s -> error (Base.Invalid_argument_ s)
   let nth l n = try Ok (nth l n) with Failure s -> error (Base.Invalid_argument_ s)
   let for_all2 f l1 l2 =
      try Ok (for_all2 f l1 l2)
      with Invalid_argument s -> error (Base.Invalid_argument_ s)
   let exists2 f l1 l2 =
      try Ok (exists2 f l1 l2)
      with Invalid_argument s -> error (Base.Invalid_argument_ s)
   let find f l = try Ok (find f l) with Not_found -> error Base.Not_found_
   let find_e f l =
      let rec find_e f = function
         | [] -> error Base.Not_found_
         | x::xs -> f x >>= function
            | true -> Ok x
            | false -> find_e f xs
      in
      find_e f l
   let assoc f l = try Ok (assoc f l) with Not_found -> error Base.Not_found_
   let assq f l = try Ok (assq f l) with Not_found -> error Base.Not_found_
   let rev_map_e f l =
      fold_left
         (fun ys x ->
            ys >>= fun ys ->
            f x >>= fun y ->
            Ok (y :: ys))
         (Ok [])
         l
   let map_e f l = rev_map_e f l >>= fun l -> Ok (rev l)
   let rev_mapi_e f l =
      fold_left
         (fun ysi x ->
            ysi >>= fun (ys, i) ->
            f i x >>= fun y ->
            Ok (y :: ys, i + 1))
         (Ok ([], 0))
         l
      >>= fun (l, _) ->
      Ok l
   let mapi_e f l =
      fold_left
         (fun ysi x ->
            ysi >>= fun (ys, i) ->
            f i x >>= fun y ->
            Ok (y :: ys, i + 1))
         (Ok ([], 0))
         l
      >>= fun (l, _) ->
      Ok (List.rev l)
   let map2_e f xs ys =
      try
         fold_left2
            (fun zs x y ->
               zs >>= fun zs ->
               f x y >>= fun z ->
               Ok (z :: zs))
            (Ok [])
            xs ys
         >>= fun zs ->
         Ok (rev zs)
      with Invalid_argument s -> error (Base.Invalid_argument_ s)
   let mapi2_e f xs ys =
      try
         fold_left2
            (fun zsi x y ->
               zsi >>= fun (zs, i) ->
               f i x y >>= fun z ->
               Ok (z :: zs, i + 1))
            (Ok ([], 0))
            xs ys
         >>= fun (zs, _) ->
         Ok (rev zs)
      with Invalid_argument s -> error (Base.Invalid_argument_ s)

end


module Stdlib
   (Error : Traced.Sigs.ERROR)
   (Trace : Traced.Sigs.TRACE with type error := Error.error)
   (Traced : Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result)
   (User : sig
      val invalid_arg_user_info : string Error.user_t
      val not_found_user_info : unit Error.user_t
   end)
: Sigs.STDLIB
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a traced := ('a, Trace.trace) result
= struct

   module Base = MkBase (Error) (Trace) (Traced) (User)

   include Base

   module List = List (Base)

end
