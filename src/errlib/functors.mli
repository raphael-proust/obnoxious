
module Stdlib
   (Error : Traced.Sigs.ERROR)
   (Trace : Traced.Sigs.TRACE with type error := Error.error)
   (Traced : Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result)
   (User : sig
      val invalid_arg_user_info : string Error.user_t
      val not_found_user_info : unit Error.user_t
   end)
: Sigs.STDLIB
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a traced := ('a, Trace.trace) result
