(** Modules with the [USER] signature are for the plugin mechanism: users can
    attach metadata of their choice to errors. *)
module type USER = sig
   type 'a t
   val exception_user_info : exn t
end

module type ERROR = sig

   (* for constraining/substituting with [USER.t] *)
   type 'a user_t

   (** Errors. Errors that are declared must be registered exactly once with
       {!register_error}. *)
   type error = ..

   (** [register_error ~id ~payload_of_error ~error_of_payload ~pp_payload]
       registers an error.

       The [id] argument is used to enforce good use of the registration system:
       an error cannot be registered twice.
       The [payload_of_error] and [error_of_payload] are conversion functions to
       and from the error type and the type of arguments carried by the error.
       E.g., for an error, say, [type error += Invalid_argument_ of string] the
       payload is a [string] and the arguments are
       [function | Invalid_argument_ s -> s | _ -> None] and
       [fun s -> Invalid_argument_ s].
       The [pp_payload] argument is a pretty-printer for the payload of the
       error. It determins how the error is pretty-printed in {!pp_error}.

       raises [Invalid_argument] if the id is already used. This is only to
       enforce good usage of the error registration system.

       Because it raises [Invalid_argument] on double registration, note that you
       must apply caution when registering an error within a functor: make ids
       unique based on the functor argument.
   *)
   val register_error :
    id:string ->
    payload_of_error:(error -> 'a option) ->
    error_of_payload:('a -> error) ->
    pp_payload:(Format.formatter -> 'a -> unit) ->
    user_info:'a user_t ->
    unit

   (** [pp_error fmt error] pretty-prints [error] on [fmt]. The format is
       specified per-error during the registration of errors with
       {!register_error}. *)
   val pp_error : Format.formatter -> error -> unit

   (** [internal_info] is information that can be recovered for any given error.
    *)
   type 'a a_internal_info = {
     id : string;
     payload_of_error : error -> 'a option;
     error_of_payload : 'a -> error;
     pp_payload : Format.formatter -> 'a -> unit;
     user_info : 'a user_t;
   }

   type internal_info = Info : _ a_internal_info -> internal_info

   (** [query] recovers the registered information for a given error. *)
   val query : error -> internal_info

   (** [registry] is a list of all registered error. *)
   val registry : unit -> internal_info list

   (* TODO: move [Exception] into [Traced] *)
   (** [Exception] is a generic wrapper for the exceptions. It is intended to be
       used for handling exceptions raised by code from other libraries. *)
   type error += Exception of exn

end

module type TRACE = sig

   (* for constraining/substituting with [Error.t]*)
   type error

   (** A [trace] is a structured set of [error]s. The structure of the trace
       reflects the structure of the computation that errored. *)
   type trace

   (** [top t] is the list of the top-level errors of [t]. There can be multiple
       top-level errors in [t] because parallel/concurrent computations may lead
       to multiple errors. *)
   val top : trace -> error list

   (** [worst t cmp] is the wrost top-level error in [t], according to [cmp]. If
       [e1] is worse than [e2] then [cmp e1 e2] should be [> 0].

       The intent of [worst] is to find out the most egregious of the available
       errors. For example, errors that the program cannot recover from are
       worse than errors that the program can recover from. *)
   val worst : (error -> error -> int) -> trace -> error

   (** [trace e] is a trace with a single error [e]. *)
   val trace : error -> trace

   (** [cons e t] (CONstructor Sequential) is a trace that contains [e] at
       top-level and [t] afterwards. *)
   val cons : error -> trace -> trace

   (** [conp ts] (CONstructor Parallel) is a trace that contains all the traces
       of [ts] arranged side-by-side. *)
   val conp : trace list -> trace

   (** A few invariants:
      [top (trace e)] is [e]
      [top (cons e t)] is [e]
      [top (conp ts)] is [List.map top ts]
      *)

   (** [pp_trace] is for pretty-printing traces. It displays each error in the
       trace, and presents them as structured. *)
   val pp_trace : Format.formatter -> trace -> unit

   (**/**)
   type 'a tree = E of 'a | S of 'a * 'a tree | P of 'a tree list
   val to_errors : trace -> error tree
   val of_errors : error tree -> trace
   (**/**)
end

module type TRACED = sig

   (* for constraining/substituting with [Error.t]*)
   type error

   (* for constraining/substituting with [Traced.t]*)
   type trace

   (** A ['a Traced.t] (or traced) is either an ['a] or a trace. *)
   type 'a t = ('a, trace) result

   val pp_traced : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit

   (** [return x] is [Ok x] *)
   val return : 'a -> 'a t

   (** [error x] is [Error (trace x)] *)
   val error : error -> 'a t

   (** [bind r f] is [f x] if [r] is [Ok x], and [r] otherwise. *)
   val bind : 'a t -> ('a -> 'b t) -> 'b t

   (** [>>=] is an infix [bind] *)
   val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

   (** [map r f] is [Ok (f x)] if [r] is [Ok x], and [r] otherwise. *)
   val map : 'a t -> ('a -> 'b) -> 'b t

   (** [>|=] is an infix [map] *)
   val ( >|= ) : 'a t -> ('a -> 'b) -> 'b t

   (** [both a b] is [Ok (x, y)] if [a] is [Ok x] and [b] is [Ok y]. It is
       [Error _] otherwise and the trace carried by the error carries all the
       errors that have occured.

       In short, [both] carries both of the results or it errors if either of
       the argument errors. *)
   val both : 'a t -> 'b t -> ('a * 'b) t

   (** [either a b] is
       - [Ok x] if [a] is [Ok x],
       - [Ok y] if [a] is [Error _] and [b] is [Ok y],
       - [Error _] otherwise and the trace carries both of [a]'s and [b]'s
         errors.

       In short, [either] carries either of the results or it errors if neither
       of the argument succeed. *)
   val either : 'a t -> 'a t -> 'a t

   (** [trace e t] is [t] if [t] is [Ok _], and [Error (Traced.cons e es)] if
       [t] is [Error es]. In other words, [trace] puts a new error on the
       top-level of the trace. *)
   val trace : error -> 'a t -> 'a t

   (** [salvage f t] is [t] if [t] is [Ok _] and [f es] if [t] is [Error es]. *)
   val salvage : (trace -> 'a t) -> 'a t -> 'a t

   (** [recover f t] is [x] if [t] is [Ok x] and [f es] if [t] is [Error es]. *)
   val recover : (trace -> 'a) -> 'a t -> 'a

   (** [recover_map ~f ~recover t] is [Ok (f x)] if [t] is [Ok x] and [f es] if
       [t] is [Error es]. *)
   val recover_map : f:('a -> 'b) -> recover:(trace -> 'b) -> 'a t -> 'b

   (** [recover_error ?fmt cmp r t] is [v] if [t] is [Ok v] and [r (Traced.worst
       cmp t)] otherwise. The argument [fmt] is used to pretty-print the trace
       if provided. *)
   val recover_error : ?fmt:Format.formatter -> (error -> error -> int) -> (error -> 'a) -> 'a t -> 'a

   (** [salvage_error ?fmt cmp s t] is [t] if [t] is [Ok _] and [s (Traced.worst
       cmp t)] otherwise. The argument [fmt] is used to pretty-print the trace
       if provided. *)
   val salvage_error : ?fmt:Format.formatter -> (error -> error -> int) -> (error -> 'a t) -> 'a t -> 'a t

   (** [catch_exists catcher t] is [t] if [t] is [Ok _]. Otherwise [catcher] is
       called with each exception at the top of the trace (in an unspecified
       order) and if any returns [Some u] then it is [u]. Otherwise it is [t].

       In other words, [catch_exists] returns the original result except if the
       original result is [Error] and one of its top errors is caught by the
       [catcher]. *)
   val catch_exists : (error -> 'a t option) -> 'a t -> 'a t

   (** [catch_each remove default t] takes different value depending on [t]:
       - if [t] is [Ok _], then it is [t]
       - if [t] is [Error trace], then
          - for each [e] top error of [trace] such that [remove e], the subtree
            of that [e] is removed from [trace],
          - whatever remains of the trace is returned as an error,
          - if nothing remains of the trace then [Ok default] is returned. *)
   val catch_each : (error -> bool) -> 'a -> 'a t -> 'a t

   (** [catch_fold folder init t] takes a different value depending on [t]:
       - if [t] is [Ok _], then it is [t],
       - if [t] is [Error trace], then
          - [folder]/[init] is folded over the top errors of [trace] yielding a
            [v] and [Ok v] is returned,
          - if at any point of the folding, [folder] returns [Some (Error _)]
            then this new error is returned,
          - if at any step of the folding, [folder] returns [None] then the
            final result is the original [t]. *)
   val catch_fold : (error -> 'a -> 'a t option) -> 'a -> 'a t -> 'a t

   (** [handle f] is either [Ok x] if [f ()] evaluates to [x] without raising an
       exception. It is [Error (error (Error.Exception e))] if [f ()] raises [e]
       and [e] is non-fatal. It raises [e] if [f ()] raises [e] and [e] is
       fatal. Fatal exceptions are [Assert_failure _], [Failure _],
       [Out_of_memory], [Stack_overflow], and [Undefined_recursive_module _]. *)
   val handle : (unit -> 'a t) -> 'a t

   (**/**)
   val of_trace : trace -> 'a t
   (**/**)
end
