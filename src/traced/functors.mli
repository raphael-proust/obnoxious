module Error
   (User : Sigs.USER)
: Sigs.ERROR
   with type 'a user_t = 'a User.t

module Trace
   (Error : Sigs.ERROR)
: Sigs.TRACE
   with type error := Error.error

module Traced
   (Error : Sigs.ERROR)
   (Trace : Sigs.TRACE with type error := Error.error)
: Sigs.TRACED
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a t = ('a, Trace.trace) result
