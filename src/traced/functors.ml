module Error (User : Sigs.USER)
: Sigs.ERROR
   with type 'a user_t = 'a User.t
= struct

   type error = ..

   type 'a user_t = 'a User.t

   type 'a a_internal_info = {
      id: string;
      payload_of_error: error -> 'a option;
      error_of_payload: 'a -> error;
      pp_payload: Format.formatter -> 'a -> unit;
      user_info: 'a User.t;
   }

   type internal_info = Info : _ a_internal_info -> internal_info

   let error_registry : internal_info list ref = ref []

   let registry () = !error_registry

   let query e =
      let rec query = function
         | [] -> raise Not_found (* TODO: better *)
         | (Info r as info_r) :: rs -> (
            match r.payload_of_error e with
            | Some _ -> info_r
            | None -> query rs )
      in
      query !error_registry

   let pp_error fmt error =
      List.iter
         (fun (Info { payload_of_error; pp_payload; _ }) ->
            match payload_of_error error with
            | None -> ()
            | Some p -> pp_payload fmt p)
         !error_registry

   let register_error ~id ~payload_of_error ~error_of_payload ~pp_payload ~user_info =
      if List.exists (fun (Info info) -> info.id = id) !error_registry then
         raise (Invalid_argument "Error.register")
      else
         error_registry :=
            Info { id; payload_of_error; error_of_payload; pp_payload; user_info }
            :: !error_registry

   type error += Exception of exn

   let () =
      register_error ~id:"exception"
         ~payload_of_error:(function Exception exc -> Some exc | _ -> None)
         ~error_of_payload:(fun exc -> Exception exc)
         ~pp_payload:(fun fmt exc -> Format.fprintf fmt "Exception: %s" (Printexc.to_string exc))
         ~user_info:User.exception_user_info

end

(* version of Sigs.TRACE with concrete trace type *)
module type FRIENDLY_TRACE
= sig

   type error

   type 'a tree =
      | E of 'a
      | S of 'a * 'a tree
      | P of 'a tree list
   type trace = error tree

   include Sigs.TRACE
      with type error := error
      and type 'a tree := 'a tree
      and type trace := trace

end

module Trace (Error : Sigs.ERROR)
: FRIENDLY_TRACE with type error = Error.error
= struct

   type error = Error.error
   type 'a tree =
      | E of 'a
      | S of 'a * 'a tree
      | P of 'a tree list
   type trace = error tree

   let flatten_top ts =
      List.fold_left
         (fun acc n -> match n with
            | E _ | S _ -> n :: acc
            | P ts -> List.rev_append ts acc)
         []
         ts

   let trace error = E error
   let cons error trace = S (error, trace)
   let conp traces = P (flatten_top traces)

   let top = function
      | E e -> [ e ]
      | S (e, _) -> [ e ]
      | P ts ->
            List.fold_left
               (fun acc t -> match t with
                  | E e | S (e, _) -> e :: acc
                  | P _ -> assert false)
               []
               ts

   let worst cmp t =
      match t with
      | E e -> e
      | S (e, _) -> e
      | P ts ->
         match List.flatten (List.map top ts) with
         | [] -> assert false
         | e :: es ->
            List.fold_left
               (fun worst error -> if cmp worst error < 0 then error else worst)
               e
               es

   let rec pp_trace fmt =
      let open Format in
      function
      | E error -> Error.pp_error fmt error
      | S (error, trace) ->
         Error.pp_error fmt error;
         pp_print_newline fmt ();
         pp_open_vbox fmt 2;
         pp_trace fmt trace;
         pp_close_box fmt ()
      | P traces -> pp_print_list ~pp_sep:pp_print_newline pp_trace fmt traces

   let to_errors t = t
   let of_errors t = t
end

module Traced
   (Error : Sigs.ERROR)
   (Trace : Sigs.TRACE with type error := Error.error)
: Sigs.TRACED
   with type error = Error.error
   and type trace = Trace.trace
   and type 'a t = ('a, Trace.trace) result
= struct

   type error = Error.error
   type trace = Trace.trace
   type 'a t = ('a, trace) result

   let pp_traced pp_ok fmt = function
      | Ok ok -> Format.fprintf fmt "Ok(%a)" pp_ok ok
      | Error trace -> Format.fprintf fmt "Error(%a)" Trace.pp_trace trace

   let return x = Ok x
   let error error = Error (Trace.trace error)
   let of_trace trace = Error trace

   let bind v f =
      match v with
      | Ok v -> f v
      | Error trace -> Error trace
   let ( >>= ) v f = bind v f

   let map v f =
      match v with
      | Ok v -> Ok (f v)
      | Error trace -> Error trace
   let ( >|= ) v f = map v f

   let both ra rb =
      match ra, rb with
      | Ok va, Ok vb -> Ok (va, vb)
      | Ok _, Error trace | Error trace, Ok _ -> Error trace
      | Error ta, Error tb -> Error (Trace.conp [ta; tb])

   let either ra rb =
      match ra, rb with
      | Ok v, _ | Error _, Ok v -> Ok v
      | Error ta, Error tb -> Error (Trace.conp [ta; tb])

   let trace error = function
      | Ok ok -> Ok ok
      | Error trace -> Error (Trace.cons error trace)

   let handle f =
      try
         f ()
      with
      | (Assert_failure _
        | Failure _
        | Out_of_memory
        | Stack_overflow
        | Undefined_recursive_module _)
        as exc -> raise exc
      | exc -> Error (Trace.trace (Error.Exception exc))

   let salvage salvage x =
      match x with
      | Ok _ -> x
      | Error trace -> salvage trace

   let recover recover x =
      match x with
      | Ok v -> v
      | Error trace -> recover trace

   let recover_map ~f ~recover x =
      match x with
      | Ok v -> f v
      | Error trace -> recover trace

   let recover_error ?fmt cmp r x =
      match x with
      | Ok v -> v
      | Error trace ->
            begin match fmt with
            | None -> ()
            | Some fmt -> Trace.pp_trace fmt trace
            end;
            r @@ Trace.worst cmp trace

   let salvage_error ?fmt cmp s x =
      match x with
      | Ok _ -> x
      | Error trace ->
            begin match fmt with
            | None -> ()
            | Some fmt -> Trace.pp_trace fmt trace
            end;
            s @@ Trace.worst cmp trace

   let catch_exists catcher = function
      | Ok _ as ok -> ok
      | Error trace as err ->
            let tops = Trace.top trace in
            let rec aux = function
               | [] -> err
               | top::tops ->
                     match catcher top with
                     | None -> aux tops
                     | Some r -> r
            in
            aux tops

   let catch_each catcher default = function
      | Ok _ as ok -> ok
      | Error trace as err ->
            match Trace.to_errors trace with
            | E e
            | S (e, _) -> if catcher e then Ok default else err
            | P ts ->
               let rec aux acc = function
                  | [] -> begin match acc with
                     | [] -> Ok default
                     | [ subtrace] -> Error (Trace.of_errors subtrace)
                     | _ :: _ -> Error Trace.(of_errors @@ P acc)
                  end
                  | (Trace.E e | Trace.S (e, _)) as subtrace :: remainder ->
                        let acc = if catcher e then acc else subtrace :: acc in
                        aux acc remainder
                  | Trace.P _ :: _ -> assert false
               in
               aux [] ts

   let catch_fold folder init = function
      | Ok _ as ok -> ok
      | Error trace as err ->
            let tops = Trace.top trace in
            let rec fold acc = function
               | [] -> Ok acc
               | e::es -> match folder e acc with
                  | None -> err
                  | Some (Error _ as new_err) -> new_err
                  | Some (Ok acc) -> fold acc es
            in
            fold init tops
end


