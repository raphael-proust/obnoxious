
module type PROMISE = sig

   (* for constraining/substituting with [Error.t]*)
   type error

   (* for constraining/substituting with [Traced.t]*)
   type trace

   (** A ['a Promise.t] (or promise) is a result in an Lwt promise. *)
   type 'a t = ('a, trace) result Lwt.t

   val return : 'a -> 'a t

   val error : error -> 'a t

   val bind : 'a t -> ('a -> 'b t) -> 'b t

   val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

   val map : 'a t -> ('a -> 'b) -> 'b t

   val ( >|= ) : 'a t -> ('a -> 'b) -> 'b t

   val both : 'a t -> 'b t -> ('a * 'b) t

   val trace : error -> 'a t -> 'a t

   val salvage : (trace -> 'a t) -> 'a t -> 'a t

   val recover : (trace -> 'a Lwt.t) -> 'a t -> 'a Lwt.t

   val recover_map : f:('a -> 'b Lwt.t) -> recover:(trace -> 'b Lwt.t) -> 'a t -> 'b Lwt.t

   val handle : (unit -> 'a t) -> 'a t
   (** Catchs non-fatal exceptions and wraps them in [Exception] *)

   (**/**)
   val of_trace : trace -> 'a t
   (**/**)
end

module type EXTENDED = sig

   (* for constraining/substituting with [Error.t] *)
   type error = ..

   (* for constraining/substituting with [Traced.t] *)
   type 'a traced

   (* for constraining/substituting with [Promise.t] *)
   type 'a promise

   (* for constraining/substituting with [Traced.t] *)
   type trace

   (** Simple conversion between result and promise *)
   val promise : 'a traced -> 'a promise
   val traced : 'a promise -> 'a traced Lwt.t

   module Infix : sig
      (** [|] is for "nothing", [?] is for traced, [=] is for lwt, [?=] is for
          promise (remember that an ['a promise] is an ['a traced Lwt.t])
          [>ab] where [a] and [b] are either of the mark above is for a bind/map
          style operator where the value type is described by [a] and the return
          type of the function is described by [b]. *)
      val ( >=| ) : 'a Lwt.t -> ('a -> 'b) -> 'b Lwt.t
      val ( >== ) : 'a Lwt.t -> ('a -> 'b Lwt.t) -> 'b Lwt.t
      val ( >=? ) : 'a Lwt.t -> ('a -> 'b traced) -> 'b promise
      val ( >==? ) : 'a Lwt.t -> ('a -> 'b promise) -> 'b promise
      val ( >?| ) : 'a traced -> ('a -> 'b) -> 'b traced
      val ( >?= ) : 'a traced -> ('a -> 'b Lwt.t) -> 'b promise
      val ( >?? ) : 'a traced -> ('a -> 'b traced) -> 'b traced
      val ( >??= ) : 'a traced -> ('a -> 'b promise) -> 'b promise
      val ( >?=| ) : 'a promise -> ('a -> 'b) -> 'b promise
      val ( >?== ) : 'a promise -> ('a -> 'b Lwt.t) -> 'b promise
      val ( >?=? ) : 'a promise -> ('a -> 'b traced) -> 'b promise
      val ( >?=?= ) : 'a promise -> ('a -> 'b promise) -> 'b promise
   end

   module Option : sig
      val unopt : 'a option -> error -> 'a traced
      val opt : 'a traced -> 'a option
   end

   module List : sig

      val map : 'a list -> ('a -> 'b traced) -> 'b list traced
      val mapi : 'a list -> (int -> 'a -> 'b traced) -> 'b list traced

      val map_s : 'a list -> ('a -> 'b promise) -> 'b list promise
      val mapi_s : 'a list -> (int -> 'a -> 'b promise) -> 'b list promise
      val map_p : 'a list -> ('a -> 'b promise) -> 'b list promise
      val mapi_p : 'a list -> (int -> 'a -> 'b promise) -> 'b list promise

      (* TODO: rev_map and friends *)
      (* TODO: map2 and friends *)

      val iter : 'a list -> ('a -> unit traced) -> unit traced
      val iteri : 'a list -> (int -> 'a -> unit traced) -> unit traced

      val iter_s : 'a list -> ('a -> unit promise) -> unit promise
      val iteri_s : 'a list -> (int -> 'a -> unit promise) -> unit promise
      val iter_p : 'a list -> ('a -> unit promise) -> unit promise
      val iteri_p : 'a list -> (int -> 'a -> unit promise) -> unit promise

      (* TODO: iter2 and friends *)

      val fold_left : ('a -> 'b -> 'a traced) -> 'a -> 'b list -> 'a traced
      val fold_right : ('a -> 'b -> 'b traced) -> 'a list -> 'b -> 'b traced

      val fold_left_s : ('a -> 'b -> 'a promise) -> 'a -> 'b list -> 'a promise
      val fold_right_s : ('a -> 'b -> 'b promise) -> 'a list -> 'b -> 'b promise

      (* TODO: fold_{left,right}2 and friends *)

   end

   module type PROMISE_TABLE = sig
    type key
    type 'a t
    val create : int -> 'a t
    val clear : 'a t -> unit
    val reset : 'a t -> unit

    val copy : 'a t -> 'a t
    val add : 'a t -> key -> 'a promise -> unit
    val remove : 'a t -> key -> unit

    type error += Not_found of key
    val find : 'a t -> key -> 'a promise

    val find_or_add : 'a t -> key -> (unit -> 'a promise) -> 'a promise

    val find_all : 'a t -> key -> 'a promise list
    val replace : 'a t -> key -> 'a promise -> unit
    val mem : 'a t -> key -> bool
    val iter_resolved : (key -> 'a -> unit) -> 'a t -> unit
    val iter_promised : (key -> 'a promise -> unit) -> 'a t -> unit
    val filter_map_inplace: (key -> 'a promise -> 'a promise option) -> 'a t -> unit

    val fold_resolved : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val fold_promised : (key -> 'a promise -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val length : 'a t -> int
    val stats: 'a t -> Hashtbl.statistics

   end

   module MakeTable(H:Hashtbl.HashedType) : PROMISE_TABLE with type key = H.t

end
