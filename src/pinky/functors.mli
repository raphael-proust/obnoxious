
module Promise
   (Error : Traced.Sigs.ERROR)
   (Trace : Traced.Sigs.TRACE with type error := Error.error)
: Sigs.PROMISE
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a t = ('a, Trace.trace) result Lwt.t

module Extended
   (Error : Traced.Sigs.ERROR)
   (Trace : Traced.Sigs.TRACE with type error := Error.error)
   (Traced : Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result)
   (Promise : Sigs.PROMISE
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result Lwt.t)
: Sigs.EXTENDED
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a traced := ('a, Trace.trace) result
   and type 'a promise := ('a, Trace.trace) result Lwt.t
