module Promise
   (Error : Traced.Sigs.ERROR)
   (Trace : Traced.Sigs.TRACE with type error := Error.error)
: Sigs.PROMISE
   with type error = Error.error
   and type trace = Trace.trace
   and type 'a t = ('a, Trace.trace) result Lwt.t
= struct

   type error = Error.error
   type trace = Trace.trace
   type 'a t = ('a, trace) result Lwt.t

   let return x = Lwt.return (Ok x)
   let error error = Lwt.return (Error (Trace.trace error))
   let of_trace trace = Lwt.return (Error trace)

   let bind v f =
      Lwt.bind v @@ function
      | Ok v -> f v
      | Error trace -> Lwt.return (Error trace)
   let ( >>= ) v f = bind v f

   let map v f =
      Lwt.bind v @@ function
      | Ok v -> Lwt.return_ok (f v)
      | Error trace -> Lwt.return (Error trace)
   let ( >|= ) v f = map v f

   let trace error v =
      Lwt.bind v @@ function
      | Ok ok -> Lwt.return (Ok ok)
      | Error trace -> Lwt.return (Error (Trace.cons error trace))

   let both pa pb =
      Lwt.bind pa @@ fun ra ->
      Lwt.bind pb @@ fun rb ->
      match ra, rb with
      | Ok va, Ok vb -> Lwt.return @@ Ok (va, vb)
      | Ok _, Error trace | Error trace, Ok _ -> Lwt.return @@ Error trace
      | Error ta, Error tb -> Lwt.return @@ Error (Trace.conp [ta; tb])

   let handle f =
      try
         f ()
      with
      | (Failure _ | Out_of_memory | Stack_overflow) as exc -> raise exc
      | exc -> Lwt.return (Error (Trace.trace (Error.Exception exc)))

   let salvage salvage x =
      Lwt.bind x @@ function
      |  Ok _ -> x
      | Error trace -> salvage trace

   let recover recover x =
      Lwt.bind x @@ function
      | Ok v -> Lwt.return v
      | Error trace -> recover trace

   let recover_map ~f ~recover x =
      Lwt.bind x @@ function
      | Ok v -> f v
      | Error trace -> recover trace
end


module Extended
   (Error : Traced.Sigs.ERROR)
   (Trace : Traced.Sigs.TRACE with type error := Error.error)
   (Traced : Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result)
   (Promise : Sigs.PROMISE
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = 'a Traced.t Lwt.t)
: Sigs.EXTENDED
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a traced := 'a Traced.t
   and type 'a promise := 'a Promise.t
= struct

   let promise (r : 'a Traced.t) : 'a Promise.t = Lwt.return r
   let traced (p : 'a Promise.t) : 'a Traced.t Lwt.t = p

   module Infix = struct
      let ( >=| ) v f = Lwt.map f v
      let ( >== ) = Lwt.bind
      let ( >=? ) = ( >=| )
      let ( >==? ) = ( >== )
      let ( >?| ) = Traced.map
      let ( >?? ) = Traced.bind
      let ( >?= ) v f = match v with Ok v -> (Lwt.bind (f v) Lwt.return_ok) | Error trace -> Lwt.return (Error trace)
      let ( >??= ) v f = match v with Ok v -> f v | Error trace -> Lwt.return (Error trace)
      let ( >?=| ) = Promise.map
      let ( >?=? ) v f = Lwt.bind v (function Ok v -> Lwt.return (f v) | Error trace -> Lwt.return (Error trace))
      let ( >?== ) v f = Lwt.bind v (function Ok v -> Lwt.bind (f v) Lwt.return_ok | Error trace -> Lwt.return (Error trace))
      let ( >?=?= ) = Promise.bind
   end

   module Option = struct
      let unopt o e : 'a Traced.t =
         match o with
         | Some v -> Traced.return v
         | None -> Traced.error e
      let opt r : 'a option =
         Traced.recover_map r ~f:(fun x -> Some x) ~recover:(fun _ -> None)
   end

   module List = struct

      let map xs f =
         let open Traced in
         List.fold_left
            (fun ys x ->
               ys >>= fun ys ->
               f x >>= fun y ->
               return (y :: ys))
            (return [])
            xs
         >>= fun ys ->
         return (List.rev ys)

      let mapi xs f =
         let open Traced in
         List.fold_left
            (fun ysi x ->
               ysi >>= fun (ys, i) ->
               f i x >>= fun y ->
               return (y :: ys, i + 1))
            (return ([], 0))
            xs
         >>= fun (ys, _) ->
         return (List.rev ys)

      let map_s xs f =
         let open Promise in
         List.fold_left
            (fun ys x ->
               ys >>= fun ys ->
               f x >>= fun y ->
               return (y :: ys))
            (return [])
            xs
         >>= fun ys ->
         return (List.rev ys)

      let mapi_s xs f =
         let open Promise in
         List.fold_left
            (fun ysi x ->
               ysi >>= fun (ys, i) ->
               f i x >>= fun y ->
               return (y :: ys, i + 1))
            (return ([], 0))
            xs
         >>= fun (ys, _) ->
         return (List.rev ys)

      let map_p xs f =
         let rec loop = function
            | [] -> Lwt.return (Ok [])
            | x :: xs -> (
               let y_p = f x in
               let ys_p = loop xs in
               Lwt.bind y_p @@ fun y_r ->
               Lwt.bind ys_p @@ fun ys_r ->
               match (y_r, ys_r) with
               | Ok y, Ok ys -> Lwt.return (Ok (y :: ys))
               | Error trace, Ok _ -> Lwt.return (Error [ trace ])
               | Ok _, (Error _ as err) -> Lwt.return err
               | Error trace, Error traces -> Lwt.return (Error (trace :: traces)) )
         in
         let open Promise in
         Lwt.bind (loop xs) @@ function
         | Ok result -> return (List.rev result)
         | Error traces -> Lwt.return (Error (Trace.conp (List.rev traces)))

      let mapi_p xs f =
         let rec loop i = function
            | [] -> Lwt.return (Ok [])
            | x :: xs -> (
               let y_p = f i x in
               let ys_p = loop (i + 1) xs in
               Lwt.bind y_p @@ fun y_r ->
               Lwt.bind ys_p @@ fun ys_r ->
               match (y_r, ys_r) with
               | Ok y, Ok ys -> Lwt.return (Ok (y :: ys))
               | Error trace, Ok _ -> Lwt.return (Error [ trace ])
               | Ok _, (Error _ as err) -> Lwt.return err
               | Error trace, Error traces -> Lwt.return (Error (trace :: traces)) )
         in
         let open Promise in
         Lwt.bind (loop 0 xs) @@ function
         | Ok result -> return (List.rev result)
         | Error traces -> Lwt.return (Error (Trace.conp (List.rev traces)))

      let iter xs f =
         let open Traced in
         let rec iter f = function
            | [] -> return ()
            | x :: xs -> f x >>= fun () -> iter f xs in
         iter f xs

      let iteri xs f =
         let open Traced in
         let rec iter f i = function
            | [] -> return ()
            | x :: xs -> f i x >>= fun () -> iter f (i + 1) xs
         in
         iter f 0 xs

      let iter_s xs f =
         let open Promise in
         let rec iter f = function
            | [] -> return ()
            | x :: xs -> f x >>= fun () -> iter f xs
         in
         iter f xs

      let iteri_s xs f =
         let open Promise in
         let rec iter f i = function
            | [] -> return ()
            | x :: xs -> f i x >>= fun () -> iter f (i + 1) xs
         in
         iter f 0 xs

      let iter_p xs f =
         let rec loop = function
            | [] -> Lwt.return (Ok ())
            | x :: xs -> (
               let y_p = f x in
               let ys_p = loop xs in
               Lwt.bind y_p @@ fun y_r ->
               Lwt.bind ys_p @@ fun ys_r ->
               match (y_r, ys_r) with
               | Ok (), Ok () -> Lwt.return (Ok ())
               | Error trace, Ok () -> Lwt.return (Error [ trace ])
               | Ok (), (Error _ as err) -> Lwt.return err
               | Error trace, Error traces -> Lwt.return (Error (trace :: traces)) )
         in
         let open Promise in
         Lwt.bind (loop xs) @@ function
         | Ok () -> return ()
         | Error traces -> Lwt.return (Error (Trace.conp (List.rev traces)))

      let iteri_p xs f =
         let rec loop i = function
            | [] -> Lwt.return (Ok ())
            | x :: xs -> (
               let y_p = f i x in
               let ys_p = loop (i + 1) xs in
               Lwt.bind y_p @@ fun y_r ->
               Lwt.bind ys_p @@ fun ys_r ->
               match (y_r, ys_r) with
               | Ok (), Ok () -> Lwt.return (Ok ())
               | Error trace, Ok () -> Lwt.return (Error [ trace ])
               | Ok (), (Error _ as err) -> Lwt.return err
               | Error trace, Error traces -> Lwt.return (Error (trace :: traces)) )
         in
         let open Promise in
         Lwt.bind (loop 0 xs) @@ function
         | Ok () -> return ()
         | Error traces -> Lwt.return (Error (Trace.conp (List.rev traces)))

      let fold_left f init xs =
         let open Traced in
         List.fold_left (fun acc x -> acc >>= fun acc -> f acc x) (return init) xs

      let fold_left_s f init xs =
         let open Promise in
         List.fold_left (fun acc x -> acc >>= fun acc -> f acc x) (return init) xs

      let fold_right f xs init =
         let open Traced in
         List.fold_right (fun x acc -> acc >>= fun acc -> f x acc) xs (return init)

      let fold_right_s f xs init =
         let open Promise in
         List.fold_right (fun x acc -> acc >>= fun acc -> f x acc) xs (return init)

   end

   module type PROMISE_TABLE = sig
    type key
    type 'a t
    val create : int -> 'a t
    val clear : 'a t -> unit
    val reset : 'a t -> unit

    val copy : 'a t -> 'a t
    val add : 'a t -> key -> 'a Promise.t -> unit
    val remove : 'a t -> key -> unit

    type Error.error += Not_found of key
    val find : 'a t -> key -> 'a Promise.t

    val find_or_add : 'a t -> key -> (unit -> 'a Promise.t) -> 'a Promise.t

    val find_all : 'a t -> key -> 'a Promise.t list
    val replace : 'a t -> key -> 'a Promise.t -> unit
    val mem : 'a t -> key -> bool
    val iter_resolved : (key -> 'a -> unit) -> 'a t -> unit
    val iter_promised : (key -> 'a Promise.t -> unit) -> 'a t -> unit
    val filter_map_inplace: (key -> 'a Promise.t -> 'a Promise.t option) -> 'a t -> unit

    val fold_resolved : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val fold_promised : (key -> 'a Promise.t -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val length : 'a t -> int
    val stats: 'a t -> Hashtbl.statistics

   end

   module MakeTable(H:Hashtbl.HashedType) = struct
      type key = H.t
      module Table = Hashtbl.Make(H)
      type 'a t = {table: 'a Promise.t Table.t; cleaners: unit Lwt.t Table.t}

      let create n = {table = Table.create n; cleaners = Table.create n}
      let clear t =
        Table.iter (fun _ cleaner -> Lwt.cancel cleaner) t.cleaners ;
        Table.iter (fun _ a -> Lwt.cancel a) t.table ;
        Table.clear t.cleaners ;
        Table.clear t.table
      let reset t =
        Table.iter (fun _ cleaner -> Lwt.cancel cleaner) t.cleaners ;
        Table.iter (fun _ a -> Lwt.cancel a) t.table ;
        Table.reset t.cleaners ;
        Table.reset t.table

      let copy {table; cleaners} = {
         table = Table.copy table;
         cleaners = Table.copy cleaners;
      }

      let add t k p =
        Table.add t.table k p ;
        match Lwt.state p with
        | Lwt.Return _ -> () (* no cleaners *)
        | Lwt.Fail _ -> assert false (* TODO, also error management in the rest *)
        | Lwt.Sleep ->
           Table.add t.cleaners k
             Lwt.Infix.(
                p >>= function
                | Ok _ ->
                   Table.remove t.cleaners k ; Lwt.return_unit
                | Error _ ->
                   Table.remove t.table k ; Table.remove t.cleaners k ; Lwt.return_unit
             )

     let remove t k =
       (match Table.find_opt t.cleaners k with
          | None -> ()
          | Some c -> Lwt.cancel c; Table.remove t.cleaners k) ;
       (match Table.find_opt t.table k with
          | None -> ()
          | Some c -> Lwt.cancel c; Table.remove t.table k)

    type Error.error += Not_found of key
    let find {table; _} k = match Table.find_opt table k with
       | Some p -> p
       | None -> Promise.error (Not_found k)

    let find_or_add ({table; _} as t) k i =
       match Table.find_opt table k with
       | Some p -> p
       | None ->
             let p = i () in
             add t k p;
             p

    let find_all {table; _} k = Table.find_all table k

    (** TODO: when replace should we cancel the existing promise? *)
    let replace {table; cleaners} k p =
        Table.replace table k p;
        Table.replace cleaners k
          Lwt.Infix.(
             p >>= function
             | Ok _ ->
                Table.remove cleaners k ; Lwt.return_unit
             | Error _ ->
                Table.remove table k ; Table.remove cleaners k ; Lwt.return_unit
          )
    let mem {table; _} k = Table.mem table k

    (* iter/fold: do we want to deal differently with Fail? *)

    let iter_resolved f {table; _} =
       Table.iter
          (fun k p -> match Lwt.state p with
             | Lwt.Return (Ok v) -> f k v
             | Lwt.Return (Error _) -> assert false
             | Lwt.Sleep | Lwt.Fail _ -> ())
          table
    let iter_promised f {table; _} = Table.iter f table

    let filter_map_inplace _ _ =
       assert false

    let fold_resolved f {table; _} init =
       Table.fold
          (fun k p acc -> match Lwt.state p with
             | Lwt.Return (Ok v) -> f k v acc
             | Lwt.Return (Error _) -> assert false
             | Lwt.Sleep | Lwt.Fail _ -> acc)
          table
          init
    let fold_promised f {table; _} init = Table.fold f table init

    let length {table; _} = Table.length table
    let stats {table; _} = Table.stats table

   end

end


