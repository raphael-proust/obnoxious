module User_encoding
: Traced.Sigs.USER
   with type 'a t = 'a Data_encoding.t

module Error : sig
   include Traced.Sigs.ERROR
      with type 'a user_t := 'a Data_encoding.t
   val encoding : error Data_encoding.t
end

module Trace : sig
   include Traced.Sigs.TRACE
      with type error := Error.error
   val encoding : trace Data_encoding.t
end

module Traced : sig
   include Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
   val encoding : 'a Data_encoding.t -> 'a t Data_encoding.t
end

module Promise : Pinky.Sigs.PROMISE with type error := Error.error and type trace := Trace.trace

module Extended
: Pinky.Sigs.EXTENDED
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a traced := 'a Traced.t
   and type 'a promise := 'a Promise.t
