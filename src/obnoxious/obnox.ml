module User_encoding
: Traced.Sigs.USER with type 'a t = 'a Data_encoding.t
= struct

   type 'a t = 'a Data_encoding.t

   let exception_user_info =
     let open Data_encoding in
     conv
        (fun e -> Printexc.to_string e)
        (fun s -> Failure s)
        string

end


module Error : sig

   include Traced.Sigs.ERROR
      with type 'a user_t = 'a User_encoding.t

   val encoding : error Data_encoding.t

end = struct

   include Traced.Functors.Error (User_encoding)

   let case_encoding_of_internal_info (Info info) =
      let open Data_encoding in
      let with_id = obj2 (req "id" (constant info.id)) (req "v" info.user_info) in
         case Json_only ~title:info.id
            (conv
               (fun payload -> ((), payload))
               (fun ((), payload) -> payload)
               with_id)
            info.payload_of_error
            info.error_of_payload

   let encoding_cache = ref None

   let set_error_encoding_cache_dirty () = encoding_cache := None

   let encoding =
      let open Data_encoding in
      delayed (fun () ->
         match !encoding_cache with
         | None ->
            let cases = List.map case_encoding_of_internal_info (registry ()) in
            let base = union cases in
            let encoding =
               dynamic_size
               @@ splitted ~json:base ~binary:(conv (Json.construct base) (Json.destruct base) json)
            in
            encoding_cache := Some encoding;
            encoding
         | Some encoding -> encoding)

   let register_error ~id ~payload_of_error ~error_of_payload ~pp_payload ~user_info =
      register_error ~id ~payload_of_error ~error_of_payload ~pp_payload ~user_info;
      set_error_encoding_cache_dirty ()

end


module Trace : sig

   include Traced.Sigs.TRACE with type error := Error.error

   val encoding : trace Data_encoding.t

end = struct
   include Traced.Functors.Trace (Error)

   let tree_encoding : Error.error tree Data_encoding.t =
      let open Data_encoding in
      mu "error-tree" (fun error_tree ->
         union [
            case ~title:"Error" (Tag 0)
               (obj1 (req "error" Error.encoding))
               (function E e -> Some e | S _ | P _ -> None)
               (fun e -> E e);
            case ~title:"Seq" (Tag 1)
               (obj2 (req "error" Error.encoding) (req "seq" error_tree))
               (function S (e, s) -> Some (e, s) | E _ | P _ -> None)
               (fun (e, s) -> S (e, s));
            case ~title:"Par" (Tag 2)
               (list (dynamic_size error_tree))
               (function P ts -> Some ts | E _ | S _ -> None)
               (fun ts -> P ts);
         ])

   let encoding = Data_encoding.conv to_errors of_errors tree_encoding

end


module Traced : sig

   include Traced.Sigs.TRACED
      with type error := Error.error
      and type trace := Trace.trace
      and type 'a t = ('a, Trace.trace) result

   val encoding : 'a Data_encoding.t -> 'a t Data_encoding.t

end = struct

   include Traced.Functors.Traced (Error) (Trace)

   let encoding (e : 'a Data_encoding.t) : 'a t Data_encoding.t =
     Data_encoding.result e Trace.encoding

end

module Promise
: Pinky.Sigs.PROMISE
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a t = ('a, Trace.trace) result Lwt.t
= Pinky.Functors.Promise (Error) (Trace)

module Extended
: Pinky.Sigs.EXTENDED
   with type error := Error.error
   and type trace := Trace.trace
   and type 'a traced := 'a Traced.t
   and type 'a promise := 'a Promise.t
= Pinky.Functors.Extended (Error) (Trace) (Traced) (Promise)
